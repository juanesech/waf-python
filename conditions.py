import boto3

client = boto3.client('waf')


class Update(object):
    action = ""
    value = ""

    def __init__(self, action, value):
        self.action = action
        self.value = value


def make_update(action_update, value_update):
    update = Update(action_update, value_update)
    return update


def create_ip_set(app):
    return client.create_ip_set(
        Name=app + '-WAF-WitheList-IPs',
        ChangeToken=client.get_change_token()['ChangeToken']
    )


def update_ip_set(ip_set_id):
    return client.update_ip_set(
        IPSetId=ip_set_id,
        ChangeToken=client.get_change_token()['ChangeToken'],
        Updates=[
            {
                'Action': 'INSERT',
                'IPSetDescriptor': {
                    'Type': 'IPV4',
                    'Value': '200.46.145.2/32'
                }
            },
        ]
    )
