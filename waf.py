#!/usr/bin/env python3
import sys
from time import sleep
from conditions import *
from rules import *
from acl import *

client = boto3.client('waf')

app_name = sys.argv[1]
acl_action = sys.argv[2]
rule_action = sys.argv[3]
ip_set = ''
rule = ''
acl = ''


def get_token(): return client.get_change_token()['ChangeToken']


ip_sets_list = client.list_ip_sets(
    Limit=100
)

for i in ip_sets_list['IPSets']:
    if i['Name'] == app_name + '-WAF-WitheList-IPs':
        ip_set = i

if not ip_set:
    print('IPSet Not found, creating')
    ip_set = create_ip_set(app_name)['IPSet']
    update_ip_set(ip_set['IPSetId'])
else:
    print('Updating IPSet')
    update_ip_set(ip_set['IPSetId'])

rules_list = client.list_rules(
    Limit=100
)

for i in rules_list['Rules']:
    if i['Name'] == app_name + '-WAF-WitheList-Rule':
        rule = i

if not rule:
    print('Rule Not found, creating')
    rule = create_rule(app_name)['Rule']
    update_rule(rule['RuleId'], ip_set['IPSetId'])
else:
    print('Updating Rule')
    update_rule(rule['RuleId'], ip_set['IPSetId'])

acls_list = client.list_web_acls(
    Limit=100
)

for i in acls_list['WebACLs']:
    if i['Name'] == app_name + '-WAF-WitheList-ACL':
        acl = i

if not acl:
    print('WebACL Not found, creating')
    acl = create_acl(app_name, 'ALLOW')['WebACL']
    print('ACL ID: ' + acl['WebACLId'])
    update_acl(rule['RuleId'], acl['WebACLId'], acl_action, rule_action)
else:
    print('Updating WebACL')
    print('ACL ID: ' + acl['WebACLId'])
    update_acl(rule['RuleId'], acl['WebACLId'], acl_action, rule_action)
    replace_acl_id(acl['WebACLId'])
sleep(40)
