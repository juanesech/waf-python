#!/usr/bin/env python3
import boto3

client = boto3.client('waf')


def create_rule(app):
    return client.create_rule(
        Name=app + '-WAF-WitheList-Rule',
        MetricName=app + 'WAFWitheListRuleMetric',
        ChangeToken=client.get_change_token()['ChangeToken']
    )


def update_rule(rule_id, ip_set_id):
    return client.update_rule(
        RuleId=rule_id,
        ChangeToken=client.get_change_token()['ChangeToken'],
        Updates=[
            {
                'Action': 'INSERT',
                'Predicate': {
                    'Negated': False,
                    'Type': 'IPMatch',
                    'DataId': ip_set_id
                }
            },
        ]
    )
