#!/usr/bin/env python3
import boto3

client = boto3.client('waf')


def create_acl(app, action):
    return client.create_web_acl(
        Name=app + '-WAF-WitheList-ACL',
        MetricName=app + 'WAFWitheListACLMetric',
        DefaultAction={
            'Type': action
        },
        ChangeToken=client.get_change_token()['ChangeToken']
    )


def update_acl(rule_id, acl_id, acl_act, rule_act):
    return client.update_web_acl(
        WebACLId=acl_id,
        ChangeToken=client.get_change_token()['ChangeToken'],
        Updates=[
            {
                'Action': 'INSERT',
                'ActivatedRule': {
                    'Priority': 1,
                    'RuleId': rule_id,
                    'Action': {
                        'Type': rule_act
                    }
                }
            },
        ],
        DefaultAction={
            'Type': acl_act
        }
    )


def replace_acl_id(acl_id):
    text = open("parameters.json").read()
    replace = text.replace('ACL_ID_REPLACE', acl_id)
    file = open("parameters.json", 'w')
    file.write(replace)
    file.close()
